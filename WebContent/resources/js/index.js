var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: []
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
        sessionStorage.removeItem('funcionarioSel');
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno: Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
        excluirFuncionario: function (id) {
            axios.delete(`/funcionarios/rs/funcionarios/${id}`)
            .then(() => this.listarFuncionarios())
            .catch(() => this.mostraAlertaErro(
                    "Erro interno: Não foi possível excluir funcionário"));
        },
        prepararEdicao(funcionario) {
            sessionStorage.setItem('funcionarioSel', JSON.stringify(funcionario));
            window.location.href = '/funcionarios/pages/novo-funcionario.html';
        },
        mostraAlertaErro(menssagem) {
            alert(menssagem);
        }
    }
});