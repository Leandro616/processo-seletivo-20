var funcionario = {
   nome: '',
   email: '',
   setor: { id: '' },
   salario: '',
   idade: '',
}

var vm = new Vue({
   el: '#app',
   data: {
      funcionario,
      setores: [],
      alertaErro: '',
      alertaSucesso: '',
      editando: Boolean,
      setorNovo: { nome: '' },
      cadastrandoSetor: Boolean
   },
   created() {
      this.listarSetores();
      this.cadastrandoSetor = false;

      if (JSON.parse(sessionStorage.getItem('funcionarioSel'))) {
         this.editando = true;
         this.funcionario = JSON.parse(sessionStorage.getItem('funcionarioSel'));
      } else {
         this.editando = false;
      }
   },
   methods: {
      salvarFuncionario() {
         if (!this.campoFuncionarioVazio() && this.funcionario.setor.id) {
            axios.post("/funcionarios/rs/funcionarios", this.funcionario)
               .then(res => {
                  this.alertaErro = '';
                  this.alertaSucesso = 'Funcionario salvo com sucesso';
                  this.limparCamposFuncionario();
               })
               .catch(err => {
                  this.mostraAlertaErro('Erro interno', err)
               })
         } else {
            this.alertaSucesso = '';
            this.alertaErro = 'Preencha todos os campos';
         }
      },
      listarSetores() {
         axios.get("/funcionarios/rs/setores")
            .then(res => {
               this.setores = res.data;
            })
            .catch(err => {
               this.mostraAlertaErro('Erro interno', err)
            })
      },
      atualizarFuncionario() {
         if (!this.campoFuncionarioVazio()) {
            let id = this.funcionario.id;

            axios.put(`/funcionarios/rs/funcionarios/${id}`, this.funcionario)
               .then(res => {
                  alert('Funcionario atualizado com sucesso!');
                  window.location.href = '/funcionarios';
               })
               .catch(err => {
                  this.mostraAlertaErro('Erro interno', err)
               })
         } else {
            this.alertaSucesso = '';
            this.alertaErro = 'Preencha todos os campos';
         }
      },
      salvarSetor() {
         if (this.setorNovo.nome) {
            axios.post('/funcionarios/rs/setores', this.setorNovo)
               .then(res => {
                  alert('Setor salvo com sucesso!');
                  window.location.href = '/funcionarios/pages/novo-funcionario.html';
               })
               .catch(err => {
                  this.mostraAlertaErro('Erro interno', err)
               })
         } else {
            this.alertaErro = 'Preencha todos os campos';
         }
      },
      campoFuncionarioVazio() {
         let vazio = false;

         Object.keys(this.funcionario).forEach((item) => {
            if (!this.funcionario[item]) {
               vazio = true;
            }
         });

         return vazio;
      },
      limparCamposFuncionario() {
         Object.keys(funcionario).forEach((item) => {
            if (item != 'setor') {
               funcionario[item] = ''
            }
         });
      },
      mostraAlertaErro(menssagem, erro) {
         alert(menssagem + " : ", erro);
      }

   },

});