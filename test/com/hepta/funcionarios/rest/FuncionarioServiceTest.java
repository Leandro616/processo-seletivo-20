package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.persistence.EntityManager;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.persistence.HibernateUtil;

import org.glassfish.grizzly.http.util.HttpStatus;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FuncionarioServiceTest extends JerseyTest {

	@BeforeEach
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@AfterEach
	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected Application configure() {
		return new ResourceConfig(FuncionarioService.class);

	}
	
	@Order(1) @Test 
	void testFuncionarioCreate() {
		Funcionario funcionario = FuncionarioBuilder.builder()
				.addNome("fulano")
				.addEmail("fulano@email.com")
				.build();
		
		Response response = target("/funcionarios").request()
			.post(Entity.entity(funcionario, MediaType.APPLICATION_JSON));
		assertEquals(HttpStatus.OK_200.getStatusCode(), response.getStatus());
	}

	@Order(2) @Test 
	void testFuncionarioRead() throws Exception {
		Response response = target("/funcionarios").request().get();
		assertEquals(HttpStatus.OK_200.getStatusCode(), response.getStatus());
	}


	@Order(3) @Test 
	void testFuncionarioUpdate() {
		Funcionario funcionario = FuncionarioBuilder.builder()
				.addNome("fulano")
				.addEmail("fulano@email.com")
				.build();

		Integer id = getUltimoId();

		Response response = target("/funcionarios/" + id).request()
			.put(Entity.entity(funcionario, MediaType.APPLICATION_JSON));
		assertEquals(HttpStatus.OK_200.getStatusCode(), response.getStatus());
	}

	@Order(4) @Test 
	void testFuncionarioDelete() {
		Integer id = getUltimoId();

		Response response = target("/funcionarios/" + id).request()
			.delete();
		assertEquals(HttpStatus.OK_200.getStatusCode(), response.getStatus());
	}

	private Integer getUltimoId() {
		EntityManager manager = HibernateUtil.getEntityManager();

		return manager
			.createQuery("from Funcionario order by id desc", Funcionario.class)
			.getResultStream()
			.findFirst()
			.get().getId();
	}

}
