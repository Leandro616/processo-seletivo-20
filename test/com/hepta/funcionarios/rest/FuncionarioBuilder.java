package com.hepta.funcionarios.rest;

import com.hepta.funcionarios.entity.Funcionario;

public class FuncionarioBuilder {
   private Funcionario funcionario;

   public FuncionarioBuilder() {
      funcionario = new Funcionario();
   }

   public static FuncionarioBuilder builder() {
      return new FuncionarioBuilder();
   }

   public FuncionarioBuilder addNome(String nome) {
      funcionario.setNome(nome);
      return this;
   }

   public FuncionarioBuilder addEmail(String email) {
      funcionario.setEmail(email);
      return this;
   }

   public FuncionarioBuilder addSalario(double salario) {
      funcionario.setSalario(salario);
      return this;
   }
   
   public FuncionarioBuilder addIdade(Integer idade) {
      funcionario.setIdade(idade);
      return this;
   }

   public Funcionario build() {
      return funcionario;
   }
}
