package com.hepta.funcionarios.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.hepta.funcionarios.entity.Setor;

public class SetorDAO {

   public void save(Setor setor) throws Exception {
      EntityManager manager = HibernateUtil.getEntityManager();
      try {
         manager.getTransaction().begin();
         manager.persist(setor);
         manager.getTransaction().commit();
      } catch (Exception e) {
         manager.getTransaction().rollback();
         throw new Exception(e);
      } finally {
         manager.close();
      }
   }

   public List<Setor> getAll() throws Exception {
      EntityManager manager = HibernateUtil.getEntityManager();
      List<Setor> setores = new ArrayList<>();
      
      try {
         manager.getTransaction().begin();
         setores = manager.createQuery("from Setor", Setor.class)
            .getResultList();
         manager.getTransaction().commit();
      } catch (Exception e) {
         manager.getTransaction().rollback();
         throw new Exception(e);
      } finally {
         manager.close();
      }

      return setores;
   }
}
