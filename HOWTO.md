# Execução da aplicação e dos testes

1. Primeiramente você deve configurar o arquivo persistence.xml (src\META-INF\persistence.xml), trocando o user e o password de acordo com o seu banco de dados: 

````xml
<property name="javax.persistence.jdbc.user" value="nome_do_usuario" />
<property name="javax.persistence.jdbc.password" value="senha" />
````

2. Crie a database com o nome 'funionarios_prova' em seu MySQL.

3. No terminal, dentro da pasta raiz do projeto, execute o comando:

````
mvn package
````
Este comando ira empacotar o código-fonte em um arquivo .war e irá executar os testes também.

4. Para executar somente os testes use o comando:
````
mvn surefire:test
````

5. Para subir a aplicação no Tomcat execute o comando:
````
mvn tomcat7:run
````
6. Acesse a aplicação em: http:localhost:8080/funcionarios